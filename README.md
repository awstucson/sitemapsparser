# Sitemaps Parser tool
The library provides tools to test & validate sitemaps. Starting point for the tool is the sitemap index but the tools can be called on individual sitemap files independently if needed. 

The library uses multiple Tasks running in parallel and provides default implementation for controlling the maximum running tasks however another implementation can be used instead

You can access the results through Result property which is IEnumerable of Tuples or by constructing the output printer and see the results in the output printer (e.g. Console.WriteLine)

## Tools
### Finder
* Allows to report sitemap entries with specific string/pattern
* Support simple string; it doesn't support regex or wild characters
* Outputs the entries matching the pattern as well as the exact sitemap file where the entry is available

```csharp
public IEnumerable<(Uri sitemapUri, Uri locationUri)> Result
```

### Counter
* Counts entries with the specified patterns
* Without specifying patterns it uses job pricing type for jobview - e.i. "/11/", "/21/", "/22/", "/31/", "/23/"
* Outputs number of entries for each of the defined pattern
```csharp
public IEnumerable<(string pattern, int count)> Result
```

### Canonical Links Verifier
* Verifies the link from sitemap with canonical link in the page
* Takes just x random links from each sitemap
* if 0 is specified then all links are verified; however be very careful with it as to verify the canonical link a web request needs to be executed for each of the entry in sitemap
* Outputs sitemap entries with different urls in sitemap and canonical
```csharp
public IEnumerable<(Uri locationUri, Uri canonicalUri)> Result
```

### Url Status Verifier
* Verifies response code for all the entries in all sitemaps; therefore this is very slow tool
* Possibility to limit to subset - for jobview limit to cloud or duration jobs only
* Uses HEAD and subsequent GET request to verify the status
* Returns the number of urls for each status code and urls which respond with different status than 200
```csharp
public IEnumerable<KeyValuePair<HttpStatusCode, int>> Result
public IEnumerable<Uri> FailedUris
```

## Samples
The folder [samples](https://github.monster.com/QaTesting/SitemapsParser/tree/master/samples) contains sample programs demonstrating how to use the tools described above

## Nuget availability
The package is available as nuget package through Monster nuget server
