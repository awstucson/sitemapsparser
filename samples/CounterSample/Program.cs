﻿using System;
using System.Collections.Generic;
using Monster.QaTesting.Common.Tasks;
using Monster.QaTesting.Tools.SitemapsParser;
using Monster.QaTesting.Tools.SitemapsParser.Processors;

namespace CounterSample
{
    class Program
    {
        private static readonly IDictionary<string, string> Sitemaps
            = new Dictionary<string, string>()
            {
                ["USJV30"] = "https://job-openings.monster.com/DYHoJtNY",
                ["USJV10"] = "http://jobview.monster.com/BYHoJtNY.ashx",
                ["CAJV30"] = "https://job-openings.monster.ca/DYHoJtNY",
                ["DEJV30"] = "https://stellenangebot.monster.de/CYHoJtNY"
            };

        private SiteMapClient _client;
        private readonly int _maxTasks;
        private readonly string _sitemapUrl;
        private readonly Action<string> _outputPrinter;

        public Program()
        {
            //All the configuration is done in this section
            //How many tasks get executed in parallel
            _maxTasks = 6;

            //Selecting sitemap from the dictionary defined above
            _sitemapUrl = Sitemaps["DEJV30"];

            //Defines where the results are printed
            _outputPrinter = Console.WriteLine;
        }

        private void InitializeClient()
        {
            _client = new SiteMapClient(new Uri(_sitemapUrl),
                () => MaxParallelTasksExecutor.CreateInsTasksExecutor(_maxTasks));
            _client.LoadSiteIndexAsync().Wait();
        }

        private void CountJobTypes()
        {
            //You can define different structure for grouping and counting the jobs
            //Default is using the job types /11/, /21/ etc.
            var counter = new Counter(_outputPrinter);
            _client.ProcessAllSitemaps(counter);
            counter.PrintResults();
        }


        static void Main(string[] args)
        {
            var program = new Program();
            program.InitializeClient();

            program.CountJobTypes();

            Close();
        }

        private static void Close()
        {
            Console.WriteLine("Press any key");
            Console.ReadLine();
        }
    }
}
