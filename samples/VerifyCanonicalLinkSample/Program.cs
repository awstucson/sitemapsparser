﻿using System;
using System.Collections.Generic;
using Monster.QaTesting.Common.Tasks;
using Monster.QaTesting.Tools.SitemapsParser;
using Monster.QaTesting.Tools.SitemapsParser.Processors;

namespace VerifyCanonicalLinkSample
{
    class Program
    {
        private static readonly IDictionary<string, string> Sitemaps
            = new Dictionary<string, string>()
            {
                ["USJV30"] = "https://job-openings.monster.com/DYHoJtNY",
                ["USJV10"] = "http://jobview.monster.com/BYHoJtNY.ashx",
                ["CAJV30"] = "https://job-openings.monster.ca/DYHoJtNY",
                ["DEJV30"] = "https://stellenangebot.monster.de/CYHoJtNY"
            };

        private SiteMapClient _client;
        private readonly int _maxTasks;
        private readonly string _sitemapUrl;
        private readonly Action<string> _outputPrinter;
        private readonly int _numOfRandomJobs;

        public Program()
        {
            //All the configuration is done in this section
            //How many tasks get executed in parallel
            _maxTasks = 6;

            //Selecting sitemap from the dictionary defined above
            _sitemapUrl = Sitemaps["DEJV30"];

            //Defines where the results are printed
            _outputPrinter = Console.WriteLine;

            //Defines how many jobs from each sitemap is checked
            //if 0 all jobs are checked
            _numOfRandomJobs = 5;
        }

        private void InitializeClient()
        {
            _client = new SiteMapClient(new Uri(_sitemapUrl),
                () => MaxParallelTasksExecutor.CreateInsTasksExecutor(_maxTasks));
            _client.LoadSiteIndexAsync().Wait();
        }

        private void VerifyCanonical()
        {
            var verifier = new CanonicalLinkVerifier(_numOfRandomJobs, _outputPrinter);
            _client.ProcessAllSitemaps(verifier);
        }

        static void Main(string[] args)
        {
            var program = new Program();
            program.InitializeClient();

            program.VerifyCanonical();

            Close();
        }

        private static void Close()
        {
            Console.WriteLine("Press any key");
            Console.ReadLine();
        }
    }
}
