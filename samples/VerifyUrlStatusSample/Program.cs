﻿using System;
using System.Collections.Generic;
using System.IO;
using Monster.QaTesting.Common.Tasks;
using Monster.QaTesting.Tools.SitemapsParser;
using Monster.QaTesting.Tools.SitemapsParser.Processors;

namespace VerifyUrlStatusSample
{
    class Program
    {
        private static readonly IDictionary<string, string> Sitemaps
            = new Dictionary<string, string>()
            {
                ["USJV30"] = "https://job-openings.monster.com/DYHoJtNY",
                ["USJV10"] = "http://jobview.monster.com/BYHoJtNY.ashx",
                ["CAJV30"] = "https://job-openings.monster.ca/DYHoJtNY",
                ["DEJV30"] = "https://stellenangebot.monster.de/CYHoJtNY"
            };

        private SiteMapClient _client;
        private readonly int _maxTasks;
        private readonly string _sitemapUrl;
        private readonly Action<string> _outputPrinter;
        private readonly string _pathToFileForJobsWithNotOkStatus;

        public Program()
        {
            //All the configuration is done in this section
            //How many tasks get executed in parallel
            _maxTasks = 6;

            //Selecting sitemap from the dictionary defined above
            _sitemapUrl = Sitemaps["DEJV30"];

            //Defines where the results are printed
            _outputPrinter = Console.WriteLine;

            //Defines the path to file which can be optionally used to save the results
            _pathToFileForJobsWithNotOkStatus = @"c:\jobs_notok_status.txt";
        }

        private void InitializeClient()
        {
            _client = new SiteMapClient(new Uri(_sitemapUrl),
                () => MaxParallelTasksExecutor.CreateInsTasksExecutor(_maxTasks));
            _client.LoadSiteIndexAsync().Wait();
        }

        private void VerifyUrlStatusCodes()
        {
            var verifier = new UrlStatusVerifier(
                () => MaxParallelTasksExecutor.CreateInsTasksExecutor(_maxTasks), _outputPrinter);

            _client.ProcessAllSitemaps(verifier);
        }

        private void VerifyPpcJobStatus()
        {
            var verifier = new UrlStatusVerifier(
                () => MaxParallelTasksExecutor.CreateInsTasksExecutor(_maxTasks), _outputPrinter);

            _client.ProcessCloudJobsOnly(verifier);

            using (var outfile = new StreamWriter(_pathToFileForJobsWithNotOkStatus))
            {
                foreach (var uri in verifier.FailedUris)
                {
                    outfile.Write(uri);
                }
            }
        }

        static void Main(string[] args)
        {
            var program = new Program();
            program.InitializeClient();

            //Verifies the response codes for PPC jobs and saves jobs with not ok status in a file
            program.VerifyPpcJobStatus();
            Close();
        }

        private static void Close()
        {
            Console.WriteLine("Press any key");
            Console.ReadLine();
        }
    }
}
