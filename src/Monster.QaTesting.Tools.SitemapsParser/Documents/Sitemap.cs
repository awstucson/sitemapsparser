﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Monster.QaTesting.Tools.SitemapsParser.Documents
{
    public class Sitemap : SitemapBase
    {
        public IEnumerable<Uri> LocUris => IsValid
            ? (from url in Document.Root?.Elements()
               from loc in url.Elements(Ns + "loc")
               select new Uri(loc.Value)) : Enumerable.Empty<Uri>();

        public IEnumerable<DateTime> LastModDates => IsValid
            ? (from url in Document.Root?.Elements()
               from lastmod in url.Elements(Ns + "lastmod")
               select DateTime.Parse(lastmod.Value))
            : Enumerable.Empty<DateTime>();

        public Sitemap(XDocument sitemap) : base(sitemap, "urlset")
        {
        }

        public Sitemap(string sitemap) : base(sitemap, "urlset")
        {
        }
    }
}
