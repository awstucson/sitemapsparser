﻿using System.Diagnostics;
using System.Xml.Linq;

namespace Monster.QaTesting.Tools.SitemapsParser.Documents
{
    public abstract class SitemapBase
    {
        protected static readonly XNamespace Ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
        protected readonly XDocument Document;
        private readonly string _rootElementName;

        protected bool IsValid => (Document != null) && (Document.Root?.Name == Ns + _rootElementName);

        public XDocument FullDocument => IsValid ? new XDocument(Document) : new XDocument();

        protected SitemapBase(XDocument document, string rootElementname)
        {
            Document = document;
            _rootElementName = rootElementname;
        }

        protected SitemapBase(string document, string rootElementName)
        {
            _rootElementName = rootElementName;
            try
            {
                var sitemapXml = XDocument.Parse(document);
                Document = sitemapXml;
            }
            catch (System.Xml.XmlException ex)
            {
                Trace.WriteLine($"Exception when parsing: {document}");
                Trace.WriteLine($"Exception details: {ex}");
                throw;
            }
        }
    }
}
