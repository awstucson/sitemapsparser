﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Monster.QaTesting.Tools.SitemapsParser.Documents
{
    public class SitemapIndex : SitemapBase
    {
        public IEnumerable<Uri> SitemapsUris => IsValid
            ? (from sitemap in Document.Root?.Elements()
               from sitemaploc in sitemap.Elements(Ns + "loc")
               select new Uri(sitemaploc.Value)) : Enumerable.Empty<Uri>();

        public IEnumerable<DateTime> LastModDates => IsValid
            ? (from sitemap in Document.Root?.Elements()
               from sitemapLastMod in sitemap.Elements(Ns + "lastmod")
               select DateTime.Parse(sitemapLastMod.Value))
            : Enumerable.Empty<DateTime>();

        public SitemapIndex(XDocument sitemapIndex) : base(sitemapIndex, "sitemapindex ")
        {
        }

        public SitemapIndex(string sitemapIndex) : base(sitemapIndex, "sitemapindex")
        {
        }
    }
}
