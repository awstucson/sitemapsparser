﻿using HtmlAgilityPack;
using Monster.QaTesting.Common;
using Monster.QaTesting.Tools.SitemapsParser.Documents;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;

namespace Monster.QaTesting.Tools.SitemapsParser.Processors
{
    public class CanonicalLinkVerifier : ISitemapProcessor
    {
        private readonly RandomChoice _rndChoice;
        private readonly Action<string> _outputPrinter;
        private readonly ConcurrentBag<(Uri locationUri, Uri canonicalUri)> _findings;

        public IEnumerable<(Uri locationUri, Uri canonicalUri)> Result => _findings;

        public CanonicalLinkVerifier(int numOfTestJobs, Action<string> outputPrinter)
        {
            _outputPrinter = outputPrinter;
            _rndChoice = new RandomChoice(numOfTestJobs);
            _findings = new ConcurrentBag<(Uri locationUri, Uri canonicalUri)>();
        }

        public CanonicalLinkVerifier(int numOfTestJobs) : this(numOfTestJobs, message => { }) { }

        public void Process(Sitemap sitemap, Uri uri)
        {
            foreach (var jobUri in _rndChoice.SelectRandomChoices(sitemap.LocUris))
            {
                ProcessSingleUri(jobUri);
            }
        }

        private void ProcessSingleUri(Uri jobUri)
        {
            var statusCode = HttpStatusCode.OK;
            var web = new HtmlWeb
            {
                PostResponse = (request, response) =>
                {
                    if (response != null)
                    {
                        statusCode = response.StatusCode;
                    }
                }
            };
            var htmlDoc = web.Load(jobUri);
            if (statusCode == HttpStatusCode.OK)
            {
                VerifyCanonicalLink(htmlDoc, jobUri);
            }
            else
            {
                Trace.WriteLine($"Request to {jobUri} resulted in {statusCode}");
            }
        }


        private void VerifyCanonicalLink(HtmlDocument htmlDoc, Uri jobUri)
        {
            var canonicalHref = htmlDoc?.DocumentNode?
                .SelectSingleNode("//head//link[@rel=\"canonical\"]")?
                .Attributes["href"].Value;
            Trace.WriteLine($"Canonical: {canonicalHref}, job: {jobUri}");
            string message = null;
            if (canonicalHref == null)
            {
                _findings.Add((jobUri, new Uri("")));
                message = $"No canonical link for {jobUri}";
            }
            else if (canonicalHref != jobUri.ToString())
            {
                _findings.Add((jobUri, new Uri(canonicalHref)));
                message = $"Canonical {canonicalHref} doesn't match with {jobUri}";
            }

            if (string.IsNullOrEmpty(message)) return;
            _outputPrinter(message);
            Trace.WriteLine(message);
        }
    }
}