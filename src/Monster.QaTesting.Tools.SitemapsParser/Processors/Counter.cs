﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Monster.QaTesting.Tools.SitemapsParser.Documents;

namespace Monster.QaTesting.Tools.SitemapsParser.Processors
{
    public class Counter : ISitemapProcessor
    {
        private readonly ConcurrentDictionary<string, int> _counter;
        private readonly string[] _patterns;
        private readonly Action<string> _outputPrinter;

        public IEnumerable<(string pattern, int count)> Result
        {
            get
            {
                foreach (var entry in _counter)
                {
                    yield return (entry.Key, entry.Value);
                }
            }
        }

        public Counter(string[] patterns, Action<string> outputPrinter)
        {
            _patterns = patterns;
            _outputPrinter = outputPrinter;
            _counter = new ConcurrentDictionary<string, int>();
            InitializeCounter();
        }
        public Counter(string[] patterns) : this(patterns, (message) => { })
        {

        }
        public Counter(Action<string> outputPrinter) : this(new[] { "/11/", "/21/", "/22/", "/31/", "/23/" }, outputPrinter)
        {
        }
        public Counter() : this(new[] { "/11/", "/21/", "/22/", "/31/", "/23/" }, (message) => { })
        {
        }

        private void InitializeCounter()
        {
            foreach (var finder in _patterns)
            {
                _counter[finder] = 0;
            }
        }

        public void PrintResults()
        {
            foreach (var (pattern, count) in Result)
            {
                Trace.WriteLine($"{pattern}: {count}");
                _outputPrinter($"{pattern}: {count}");
            }
        }

        public void Process(Sitemap sitemap, Uri uri)
        {
            foreach (var pattern in _patterns)
            {
                var locs = from locuri in sitemap.LocUris
                           where locuri.ToString().Contains(pattern)
                           select locuri;
                var matches = locs.Count();
                _counter.AddOrUpdate(pattern, matches, (k, v) => v + matches);
            }
        }
    }
}
