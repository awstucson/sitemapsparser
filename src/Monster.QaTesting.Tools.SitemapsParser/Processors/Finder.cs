﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Diagnostics;
using Monster.QaTesting.Tools.SitemapsParser.Documents;

namespace Monster.QaTesting.Tools.SitemapsParser.Processors
{
    public class Finder : ISitemapProcessor
    {
        private readonly string _pattern;
        private readonly Action<string> _outputPrinter;
        private readonly ConcurrentDictionary<Uri, List<Uri>> _findings;

        public IEnumerable<(Uri sitemapUri, Uri locationUri)> Result
        {
            get
            {
                foreach (var sitemap in _findings)
                {
                    foreach (var locUri in sitemap.Value)
                    {
                        yield return (sitemap.Key, locUri);
                    }

                }
            }
        }

        public Finder(string pattern, Action<string> outputPrinter)
        {
            _pattern = pattern;
            _outputPrinter = outputPrinter;
            _findings = new ConcurrentDictionary<Uri, List<Uri>>();

        }

        public Finder(string pattern) : this(pattern, message => { }) { }

        public void Process(Sitemap sitemap, Uri uri)
        {
            var findings = new List<Uri>();
            foreach (var locUri in sitemap.LocUris)
            {
                if (!locUri.ToString().Contains(_pattern)) continue;
                findings.Add(locUri);
            }

            if (findings.Count > 0)
            {
                _findings.AddOrUpdate(uri, findings, (k, v) => v);
            }

            foreach (var foundUrl in findings)
            {
                var message = $"Sitemap: {uri}\t found: {foundUrl}";
                _outputPrinter(message);
                Trace.WriteLine(message);
            }
        }
    }
}
