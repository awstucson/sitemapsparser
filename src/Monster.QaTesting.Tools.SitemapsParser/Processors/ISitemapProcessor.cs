﻿using System;
using Monster.QaTesting.Tools.SitemapsParser.Documents;

namespace Monster.QaTesting.Tools.SitemapsParser.Processors
{
    public interface ISitemapProcessor
    {
        void Process(Sitemap sitemap, Uri uri);
    }
}
