﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Monster.QaTesting.Common.Tasks;
using Monster.QaTesting.Tools.SitemapsParser.Documents;

namespace Monster.QaTesting.Tools.SitemapsParser.Processors
{
    public class UrlStatusVerifier : ISitemapProcessor
    {
        private readonly Func<ITasksExecutor> _taskExecutorFactory;
        private readonly Action<string> _outputPrinter;
        private readonly HttpClient _httpClient;
        private readonly ConcurrentBag<Uri> _urisWithNotOkResponse;
        private readonly ConcurrentDictionary<HttpStatusCode, int> _totalCounter;

        public IEnumerable<KeyValuePair<HttpStatusCode, int>> Result => _totalCounter;
        public IEnumerable<Uri> FailedUris => _urisWithNotOkResponse;

        public UrlStatusVerifier(Func<ITasksExecutor> taskExecutorFactory,
            Action<string> outputPrinter)
        {
            _taskExecutorFactory = taskExecutorFactory;
            _outputPrinter = outputPrinter;
            _httpClient = new HttpClient();
            _urisWithNotOkResponse = new ConcurrentBag<Uri>();
            _totalCounter = new ConcurrentDictionary<HttpStatusCode, int>();
        }

        public UrlStatusVerifier(Func<ITasksExecutor> taskExecutorFactory) : this(taskExecutorFactory, message => { }) { }

        public void Process(Sitemap sitemap, Uri uri)
        {
            _outputPrinter($"Starting processing for {uri}");
            var counter = new ConcurrentDictionary<HttpStatusCode, int>();
            var tasksExecutor = _taskExecutorFactory();
            var tasks = sitemap.LocUris.Select(
                locuri => ProcessSingleUriAsync(locuri, counter));
            tasksExecutor.ExecuteTasks(tasks);
            UpdateTotalCounter(counter);
            //PrintResult(uri, counter);
        }

        private async Task ProcessSingleUriAsync(Uri uri,
            ConcurrentDictionary<HttpStatusCode, int> counter)
        {
            var request = CreateRequestMessage(uri);
            var response = await _httpClient.SendAsync(request);
            CheckResponse(counter, response);
        }

        private void UpdateTotalCounter(ConcurrentDictionary<HttpStatusCode, int> localCounter)
        {
            foreach (var entry in localCounter)
            {
                _totalCounter.AddOrUpdate(entry.Key, entry.Value, (key, oldvalue) => oldvalue + entry.Value);
            }
        }

        private void CheckResponse(ConcurrentDictionary<HttpStatusCode, int> counter,
            HttpResponseMessage response)
        {
            counter.AddOrUpdate(response.StatusCode, 1, (key, oldvalue) => oldvalue + 1);
            if (response.StatusCode == HttpStatusCode.OK) return;

            _urisWithNotOkResponse.Add(response.RequestMessage.RequestUri);
        }

        private static HttpRequestMessage CreateRequestMessage(Uri uri) =>
            new HttpRequestMessage
            {
                Method = HttpMethod.Head,
                RequestUri = uri
            };

        private void PrintFailedResponseDetails(HttpResponseMessage response) =>
            _outputPrinter($"Uri: {response.RequestMessage.RequestUri} returned {response.StatusCode}");

        private void PrintResult(Uri uri, ConcurrentDictionary<HttpStatusCode, int> counter)
        {
            _outputPrinter($"Results for {uri}");
            foreach (var entry in counter)
            {
                _outputPrinter($"StatusCode: {entry.Key} returned: {entry.Value}");
            }
        }
    }
}
