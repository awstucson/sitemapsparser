﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Monster.QaTesting.Common.Tasks;
using Monster.QaTesting.Tools.SitemapsParser.Documents;
using Monster.QaTesting.Tools.SitemapsParser.Processors;

namespace Monster.QaTesting.Tools.SitemapsParser
{
    public class SiteMapClient
    {
        private readonly HttpClient _httpClient;
        private readonly Uri _sitemapIndexUri;
        private readonly Func<ITasksExecutor> _taskExecutorFactory;
        private SitemapIndex _sitemapIndex;

        public SiteMapClient(Uri sitemapIndexUri, Func<ITasksExecutor> taskExecutorFactory)
        {
            _httpClient = new HttpClient();
            _sitemapIndexUri = sitemapIndexUri;
            _taskExecutorFactory = taskExecutorFactory;
        }

        public IEnumerable<Sitemap> GetSitemaps() =>
            _sitemapIndex.SitemapsUris.Select(uri => GetSingleSiteMapAsync(uri).Result);

        public async Task LoadSiteIndexAsync()
        {
            Trace.WriteLine($"Retrieving sitemap index from: {_sitemapIndexUri}");
            var result = await _httpClient.GetAsync(_sitemapIndexUri);
            Trace.WriteLine($"Status code for {_sitemapIndexUri} was {result.StatusCode}");
            result.EnsureSuccessStatusCode();

            var content = await result.Content.ReadAsStringAsync();
            _sitemapIndex = new SitemapIndex(content);
        }

        public async Task<Sitemap> GetSingleSiteMapAsync(Uri sitemapUri)
        {
            Trace.WriteLine($"Retrieving sitemap from: {sitemapUri}");
            var responseMessage = await _httpClient.GetAsync(sitemapUri);
            Trace.WriteLine($"Status code for {sitemapUri} was {responseMessage.StatusCode}");
            responseMessage.EnsureSuccessStatusCode();
            var responseText = await responseMessage.Content.ReadAsStringAsync();
            return new Sitemap(responseText);
        }

        public void ProcessAllSitemaps(ISitemapProcessor processor) =>
            ProcessAllSitemapsAsync(processor).Wait();

        public void ProcessCloudJobsOnly(ISitemapProcessor processor) =>
            ProcessCloudJobsOnlyAsync(processor).Wait();

        public void ProcessDurationJobsOnly(ISitemapProcessor processor) =>
            ProcessDurationJobsOnlyAsync(processor).Wait();

        public async Task ProcessAllSitemapsAsync(ISitemapProcessor processor)
        {
            var tasks = _sitemapIndex.SitemapsUris.Select(uri => ProcessSingleSitemapAsync(processor, uri));
            await ExecuteTasksAsync(tasks);
        }

        public async Task ProcessCloudJobsOnlyAsync(ISitemapProcessor processor)
        {
            var tasks = _sitemapIndex.SitemapsUris
                .Where(uri => uri.ToString().Contains("DYHoJtNY"))
                .Select(uri => ProcessSingleSitemapAsync(processor, uri));
            await ExecuteTasksAsync(tasks);
        }

        public async Task ProcessDurationJobsOnlyAsync(ISitemapProcessor processor)
        {
            var tasks = _sitemapIndex.SitemapsUris
                .Where(uri => uri.ToString().Contains("CYHoJtNY"))
                .Select(uri => ProcessSingleSitemapAsync(processor, uri));
            await ExecuteTasksAsync(tasks);
        }

        private async Task ExecuteTasksAsync(IEnumerable<Task> tasks)
        {
            var taskExecutor = _taskExecutorFactory();
            await taskExecutor.ExecuteTasksAsync(tasks);
        }

        private async Task ProcessSingleSitemapAsync(ISitemapProcessor processor, Uri uri)
        {
            var sitemap = await GetSingleSiteMapAsync(uri);
            Trace.WriteLine($"Calling processor for {uri}");
            processor.Process(sitemap, uri);
        }
    }
}